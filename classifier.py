import numpy as np
import matplotlib.pyplot as plt
import sklearn.metrics

class Classifier:

    def __init__(self, comparator):
        """
        :param comparator: класс-компаратор. Должен иметь метод run()
        """
        self.comparator = comparator
        self.dim = 0

    def loadData(self, data_path, labels_path):
        """
        Загрузка данных из npy-файлов
        :param data_path: имя файла признаков
        :param labels_path: имя файла меток
        :return:
        """
        try:
            print('Loading file ' + data_path)
            self.data = np.load(data_path)
            print('Loading file ' + labels_path)
            self.labels = np.load(labels_path)
            self.dim = len(self.data)
        except:
            print("Can't load npy-file")

    def computePairs(self):
        """
        Вычислить значения компаратора для каждой пары векторов пизнаков
        :return:
        """
        self.pair_matr = np.zeros((self.dim, self.dim), np.float)
        self.true_ans_matr = np.zeros((self.dim, self.dim), np.bool)
        print('Computing pairs...')
        for i in range(0, self.dim):
            for j in range(i, self.dim):
                self.pair_matr[i][j] = self.comparator.run(self.data[i], self.data[j])
                self.true_ans_matr[i][j] = self.labels[i] == self.labels[j]
        self.pair_arr = np.reshape(self.pair_matr, (self.dim * self.dim))
        self.true_ans_arr = np.reshape(self.true_ans_matr, (self.dim * self.dim))

    def computeMetric(self):
        """
        Вычислить значение метрик true positive rate, false positive rate (используется sklearn)
        :return:
        """
        pair_arr = np.reshape(self.pair_matr, (self.dim * self.dim))
        true_ans_arr = np.reshape(self.true_ans_matr, (self.dim * self.dim))
        self.fpr, self.tpr, self.thresholds = sklearn.metrics.roc_curve(true_ans_arr, pair_arr)

    def buildROC(self):
        """
        Построить ROC-кривую
        :return:
        """
        print('Building ROC-curve')
        plt.plot(self.fpr, self.tpr)
        plt.xlabel('FPR')
        plt.ylabel('TPR')
        plt.title('ROC')
        plt.show()

    def getTprAndThreshold(self, FPR):
        """
        Получить значения true positive rate и соответствующего порога по значению false positive rate
        :param FPR:
        :return: кортеж элементов (tpr, порог)
        """
        idx = self.findNearestIdx(self.fpr, FPR)
        return (self.tpr[idx], self.thresholds[idx])

    def findNearestIdx(self, arr, value):
        """
        Поиск индекса элемента массива, значение которого ближе всего к заданному значению
        :param arr: массив поиска
        :param value: искомый элемент
        :return: индекс элемента
        """
        idx = (np.abs(arr - value)).argmin()
        return idx