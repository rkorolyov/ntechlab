from classifier import Classifier
from comparator import Comparator
import argparse

def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('features_file', nargs='?', default='features.npy')
    parser.add_argument('labels_file', nargs='?', default='person_id.npy')
    return parser

if __name__ == "__main__":
    pars = createParser()
    namespace = pars.parse_args()
    comp = Comparator()
    cl = Classifier(comp)
    cl.loadData(namespace.features_file, namespace.labels_file)
    cl.computePairs()
    cl.computeMetric()
    cl.buildROC()
    while(True):
        print('Enter False Positive Rate. Press C to exit ')
        fpr = input()
        if fpr == 'C':
            break
        else:
            try:
                fpr = float(fpr)
                tpr, threshold = cl.getTprAndThreshold(fpr)
                print('tpr = %f, threshold = %d' % (tpr, threshold))
            except Exception:
                print('erorr!', Exception.mro())
