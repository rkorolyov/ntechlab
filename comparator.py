import numpy as np

class Comparator:

    def __init__(self):
        pass

    def run(self, vec1, vec2):
        return np.dot(vec1, vec2)